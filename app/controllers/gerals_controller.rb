class GeralsController < ApplicationController

	def track_event
		@event_tracker = EventTracker.new
		event = params[:data]
		respond_to do |format|
			format.json { render :json =>  @event_tracker.perform(event) }
		end
	end

	def track_profile
		@profile_tracker = ProfileTracker.new
		profile = params[:data]
		respond_to do |format|
			format.json { render :json =>  @profile_tracker.perform(profile) }
		end
	end

	def profile_report
		filters = {}
		params[:data][:filters] ||= []
		params[:data][:filters].each do |filter|
			filters[filter['eventType']] ||= {}
			filters[filter['eventType']][filter['property']] = filter['value']
		end

		report = {:profile => {}, :average => {}}

		params[:data][:events].each do |event|
			events = EventFinder.by_type_and_properties({
				:app_token => params[:data][:app_token],
				:type => event,
				:properties => filters[event] || {},
				:time_range => TimeRange.new(params[:data][:time_range][:from], params[:data][:time_range][:to])
			})

			events.each do |event|
				if event['external_id'] == params[:data][:externalId]
					report[:profile][event['type']] ||= 0
					report[:profile][event['type']] += 1
				end
				report[:average][event['type']] ||= 0
				report[:average][event['type']] += 1
			end
		end

		profiles = ProfileFinder.by_app_token(:app_token => params[:data][:app_token])

		report[:average].each do |key, val|
			report[:average][key] = val / profiles.count.to_f
		end

		respond_to do |format|
			format.json { render :json => report }
		end
	end

	def segmentation_report
		data = params[:data]
		report = SegmentationReport.new(data).to_json
		events = EventFinder.by_type({
			:app_token => params[:data][:app_token],
			:type => params[:data][:event_type],
			:time_range => TimeRange.new(params[:data][:time_range][:from], params[:data][:time_range][:to])
		})
		report[:events] = Collections.query_to_array(events)

		respond_to do |format|
			format.json { render :json =>  report }
		end
	end

	def people_report
		profiles = ProfileFinder.by_properties({
			:app_token => params[:data][:app_token],
			:properties => params[:data][:filters] || {}
		})
		respond_to do |format|
			format.json { render :json => Collections.query_to_array(profiles) }
		end
	end

	def funnel_report
		report = FunnelReport.new(params[:data])
		respond_to do |format|
			format.json { render :json => report.funnel }
		end
	end

	def funnel_report_profiles_at_step
		report = FunnelReport.new(params[:data])
		profiles = report.profiles_at_step(params[:data][:profiles_at_step]) 
		respond_to do |format|
			format.json { render :json => Collections.query_to_array(profiles) }
		end
	end

	def events_of_app
		respond_to do |format|
			format.json { render :json => PropertyFinder.event_types(params[:app_token]) }
		end
	end

	def properties_of_event
		respond_to do |format|
			format.json { render :json => PropertyFinder.event(params[:app_token], params[:event_type]) }
		end
	end

	def profiles_of_app
		respond_to do |format|
			format.json { render :json => ProfileFinder.by_app_token(:app_token => params[:app_token]) }
		end
	end

	def profiles_properties
		respond_to do |format|
			format.json { render :json => PropertyFinder.profiles(params[:app_token]) }
		end
	end
end
