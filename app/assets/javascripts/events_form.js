angular.module('dos')
	.controller('events_form', function($scope, $http, profilesOfApp) {

		$scope.event = {
			'app_token': window.app.token
		};

		profilesOfApp(function(profiles) {
			$scope.profiles = profiles;
		});

		$scope.properties = [];

		$scope.addProperty = function() {
			console.log("called");
			$scope.properties.push({
				name: '',
				value: ''
			});
		};

		$scope.track = function() {
			var properties = {};
			_.each($scope.properties, function(e) {
				properties[e.name] = e.value;
			});

			$scope.event.properties = properties;

			$http.post('/track_event.json', {data: $scope.event}).success(function() {
				window.alert("tracked");
			});
		};
	});