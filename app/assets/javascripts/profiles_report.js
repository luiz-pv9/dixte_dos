angular.module('dos')
	.controller('profiles_report', function($scope, $http, eventsOfApp, propertiesOfEvent, profilesOfApp) {
		$scope.externalId = null;
		$scope.eventsToCompare = [];
		$scope.filters = [];
		$scope.properties = {};
		$scope.time_range = {
			'from': '2013-01-01',
			'to': '2016-01-01'
		};

		eventsOfApp(function(events) {
			$scope.events = events;
			profilesOfApp(function(profiles) {
				$scope.profiles = profiles;
			});

			_.each($scope.events, function(event) {
				propertiesOfEvent.get(event, function(properties) {
					if(properties) {
						$scope.properties[event] = properties;
					}
				})
			});
		});

		$scope.addEvent = function() {
			$scope.eventsToCompare.push({
				eventType: ''
			});
		};

		$scope.properties = {};

		$scope.addFilter = function() {
			$scope.filters.push({
				eventType: '',
				property: '',
				value: ''
			});
		};

		$scope.propertiesOfEvent = function(eventType) {
			if(eventType && eventType != '') {
				return propertiesOfEvent.properties($scope.properties[eventType]);
			}
			return [];
		};

		$scope.valuesOfProperty = function(eventType, property) {
			if(eventType && eventType != '' && property && property != '') {
				return propertiesOfEvent.valuesOfProperty(property, $scope.properties[eventType]);
			}
			return [];
		};

		$scope.render = function() {
			var data = {
				'app_token': window.app.token,
				'externalId': $scope.externalId,
				'events': _.map($scope.eventsToCompare, function(e) { return e.eventType }),
				'filters': _.map($scope.filters, function(e) { return {
					eventType: e.eventType,
					property: e.property,
					value: e.value
				}  }),
				'time_range': {
					'from': new Date($scope.time_range.from).getTime() / 1000,
					'to': new Date($scope.time_range.to).getTime() / 1000,
				}
			};

			$http.post('/profiles_report.json', {data: data}).success(function(report) {
				$scope.renderChart('#chart-profile', 'Usuário: ' + $scope.externalId, report.profile);
				$scope.renderChart('#chart-average', 'Usuário médio', report.average);
			});
		};

		$scope.renderChart = function(elm, title, data) {
			var chartData = [];
			_.each(_.keys(data), function(key) {
				chartData.push([key, data[key]]);
			});
		    $(elm).highcharts({
		        title: {
		            text: title
		        },
		        tooltip: {
		            pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
		                    style: {
		                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		                    }
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: 'Browser share',
		            data: chartData
		        }]
		    });
		};
	});