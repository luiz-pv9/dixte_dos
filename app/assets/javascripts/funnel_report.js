angular.module('dos')
	.controller('funnel_report', function($scope, $http, eventsOfApp, propertiesOfEvent, profilesOfApp, propertiesOfProfiles) {
		$scope.filters = [];
		$scope.steps = [];
		$scope.properties = {};
		$scope.headers = [];
		$scope.profilesAtStep = 0;
		$scope.time_range = {
			'from': '2013-01-01',
			'to': '2016-01-01'
		};

		
		eventsOfApp(function(events) {
			$scope.events = events;
			profilesOfApp(function(profiles) {
				$scope.profiles = profiles;
			});

			_.each($scope.events, function(event) {
				propertiesOfEvent.get(event, function(properties) {
					if(properties) {
						$scope.properties[event] = properties;
					}
				})
			});

			propertiesOfProfiles.get(function(profiles) {
				$scope.profileProperties = profiles;
				$scope.propertiesOfProfiles = propertiesOfProfiles.properties(profiles);
			});
		});

		$scope.addStep = function() {
			$scope.steps.push({
				event: ''
			});
		};

		$scope.addFilter = function() {
			$scope.filters.push({
				eventType: '',
				property: '',
				value: ''
			});
		};

		$scope.propertiesOfEvent = function(eventType) {
			if(eventType && eventType != '') {
				return propertiesOfEvent.properties($scope.properties[eventType]);
			}
			return [];
		};

		$scope.valuesOfProperty = function(eventType, property) {
			if(eventType && eventType != '' && property && property != '') {
				return propertiesOfEvent.valuesOfProperty(property, $scope.properties[eventType]);
			}
			return [];
		};

		$scope.addHeader = function() {
			$scope.headers.push({
				name: ''
			});
		};

		$scope.funnelData = function() {
			var filters = [];
			var steps = _.map($scope.steps, function(step) {
				return step.event;
			});

			_.each($scope.filters, function(filter) {
				var index = steps.indexOf(filter.eventType);
				if(index !== -1) {
					filters[index] = filters[index] || {};
					filters[index][filter.property] = filter.value;
				}
			});

			var data = {
				'app_token': window.app.token,
				'time_range': {
					'from': new Date($scope.time_range.from).getTime() / 1000,
					'to': new Date($scope.time_range.to).getTime() / 1000,
				},
				'steps': steps,
				'filters': filters
			};
			return data;
		};

		$scope.render = function() {
			var data = $scope.funnelData();
			$http.post('/funnel_report.json', {data: data}).success(function(report) {
				$scope.renderChart('#funnel-chart', 'Funil', data.steps, report);
			});
		};

		$scope.renderPeople = function() {
			var data = $scope.funnelData();
			data.profiles_at_step = parseInt($scope.profilesAtStep);
			$http.post('/funnel_report_profiles_at_step.json', {data: data}).success(function(report) {
				$scope.people = report;
			});
		};

		$scope.renderChart = function(elm, title, steps, data) {
			var series = [];
			_.each(data, function(val, index) {
				var serie = {
					name: steps[index],
					data: []
				};
				for(var i = 0; i < steps.length; i++) {
					if(i == index) {
						serie.data[i] = val;
					} else {
						serie.data[i] = 0;
					}
				}
				series.push(serie);
			});

			$(elm).highcharts({
		        chart: {
		            type: 'column'
		        },
		        title: {
		            text: title,
		        },
		        xAxis: {
		            categories: steps
		        },
		        yAxis: {
		            min: 0,
		            title: {
		                text: 'Total'
		            }
		        },
		        plotOptions: {
		            column: {
		                pointPadding: 0.2,
		                borderWidth: 0
		            }
		        },
		        series: series
		    });
		};

	});