angular.module('dos', [])
	.factory('eventsOfApp', function($http) {
		return function(callback) {
			$http.post('/events_of_app.json', {'app_token': window.app.token}).success(
				function(hash) {
					var events = _.keys(hash.properties.type.values);
					callback(events);
				});
		};
	})
	.factory('propertiesOfEvent', function($http) {
		var poe = {};
		poe.profileProperties = null;
		$http.post('/profiles_properties.json', {'app_token': window.app.token})
		.success(function(profilesProperties) {
			poe.profileProperties = profilesProperties;
		});

		poe.get = function(eventType, callback) {
			$http.post('/properties_of_event.json', {
				'app_token': window.app.token,
				'event_type': eventType})
			.success(callback);
		};

		poe.properties = function(hash) {
			var eventProperties = _.keys(hash.properties);
			var profilesProperties = _.keys(poe.profileProperties.properties);
			return eventProperties.concat(profilesProperties.map(function(e) {
				return 'acc:' + e;
			}));
		};

		poe.valuesOfProperty = function(property, hash) {
			if(property.indexOf('acc:') == 0) {
				return _.keys(poe.profileProperties.properties[property.replace('acc:', '')].values);
			} else {
				return _.keys(hash.properties[property].values);
			}
		};

		return poe;
	})
	.factory('propertiesOfProfiles', function($http) {
		var pop = {};

		pop.get = function(callback) {
			$http.post('/profiles_properties.json', {
				'app_token': window.app.token})
			.success(callback);
		};

		pop.properties = function(hash) {
			var profilesProperties = _.keys(hash.properties);
			return profilesProperties;
		};

		pop.valuesOfProperty = function(property, hash) {
			return _.keys(hash.properties[property].values);
		};

		return pop;
	})
	.factory('profilesOfApp', function($http) {
		return function(callback) {
			$http.post('/profiles_of_app.json', {'app_token': window.app.token}).success(
				function(hash) {
					var profiles = _.map(hash, function(e) {
						return e.external_id;
					});
					callback(profiles);
				});
		}
	})
	.filter('dateFilter', function() {
		return function(val) {
			var date = new Date(val * 1000);
			return date.getDay() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
		}
	})
