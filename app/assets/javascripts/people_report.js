angular.module('dos')
	.controller('people_report', function($scope, $http, propertiesOfProfiles) {
		$scope.filters = [];
		$scope.headers = [];
		$scope.operators = ['=', '>', '<'];

		propertiesOfProfiles.get(function(profiles) {
			$scope.profileProperties = profiles;
			$scope.properties = propertiesOfProfiles.properties(profiles);
		});

		$scope.valuesOfProperty = function(property) {
			return propertiesOfProfiles.valuesOfProperty(property, $scope.profileProperties);
		}

		$scope.addFilter = function() {
			$scope.filters.push({
				property: '',
				operator: '',
				value: ''
			});
		};

		$scope.render = function() {
			var filters = {};
			_.each($scope.filters, function(filter) {
				if(filter.operator == '=') {
					filters[filter.property] = filter.value;
				} else if(filter.operator == '>') {
					filters[filter.property] = {'$gt': parseInt(filter.value)};
				} else if(filter.operator == '<') {
					filters[filter.property] = {'$lt': parseInt(filter.value)};
				}
			});

			var data = {
				app_token: window.app.token,
				filters: filters
			};

			$http.post('/people_report.json', {data: data}).success(function(report) {
				$scope.people = report;
			});
		};

		$scope.addHeader = function() {
			$scope.headers.push({
				name: ''
			});
		};
	});