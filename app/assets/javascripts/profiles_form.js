angular.module('dos')
	.controller('profiles_form', function($scope, $http) {

		$scope.profile = {
			'app_token': window.app.token
		};
		$scope.properties = [];

		$scope.addProperty = function() {
			$scope.properties.push({
				name: '',
				value: ''
			});
		};

		$scope.track = function() {
			var properties = {};
			_.each($scope.properties, function(e) {
				properties[e.name] = e.value;
				if(parseInt(e.value)) {
					properties[e.name] = parseInt(e.value);
				}
			});

			$scope.profile.properties = properties;

			$http.post('/track_profile.json', {data: $scope.profile}).success(function() {
				window.alert("tracked");
			});
		};
	});