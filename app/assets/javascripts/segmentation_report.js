angular.module('dos')
	.controller('segmentation_report', function($scope, $http, eventsOfApp, propertiesOfEvent, profilesOfApp) {
		$scope.eventsToCompare = [];
		$scope.filters = [];
		$scope.properties = {};

		$scope.eventType = '';
		$scope.segmentBy = '';
		$scope.headers = [];

		$scope.time_range = {
			'from': '2013-01-01',
			'to': '2016-01-01'
		};

		eventsOfApp(function(events) {
			$scope.events = events;
			_.each($scope.events, function(event) {
				propertiesOfEvent.get(event, function(properties) {
					if(properties) {
						$scope.properties[event] = properties;
					}
				})
			});
		});

		$scope.propertiesOfEvent = function(eventType) {
			if(eventType && eventType != '') {
				return propertiesOfEvent.properties($scope.properties[eventType]);
			}
			return [];
		};

		$scope.addHeader = function() {
			$scope.headers.push({
				property: ''
			});
		};

		$scope.render = function() {
			var data = {
				'app_token': window.app.token,
				'event_type': $scope.eventType,
				'segment_on': $scope.segmentBy,
				'steps_in': 'years',
				'time_range': {
					'from': new Date($scope.time_range.from).getTime() / 1000,
					'to': new Date($scope.time_range.to).getTime() / 1000,
				}
			};

			$http.post('/segmentation_report.json', {data: data}).success(function(report) {
				var chartData = {};
				_.each(_.keys(report.series), function(key) {
					chartData[key] = _.reduce(report.series[key], function(memo, val) {
						if(val) {
							return memo + val;
						}
						return memo;
					}, 0);
				});
				$scope.reportEvents = report.events;
				$scope.renderChart('#chart-segmentation', 'Segmentação', chartData);
			});
		};

		$scope.renderChart = function(elm, title, data) {
			var chartData = [];
			_.each(_.keys(data), function(key) {
				chartData.push([key, data[key]]);
			});
		    $(elm).highcharts({
		        title: {
		            text: title
		        },
		        tooltip: {
		            pointFormat: '{series.name}: <b>{point.y}</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
		                    style: {
		                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		                    }
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: 'Browser share',
		            data: chartData
		        }]
		    });
		};
	});