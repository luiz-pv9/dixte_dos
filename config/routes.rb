Rails.application.routes.draw do
	match '/track' => 'event_receiver#track', :via => [:get, :post]
	match '/profile' => 'profile_receiver#track', :via => [:get, :post]

	post '/track_event' => 'gerals#track_event'
	post '/track_profile' => 'gerals#track_profile'

	post '/events_of_app' => 'gerals#events_of_app'
	post '/properties_of_event' => 'gerals#properties_of_event'
	post '/profiles_of_app' => 'gerals#profiles_of_app'
	post '/profiles_properties' => 'gerals#profiles_properties'

	post '/profiles_report' => 'gerals#profile_report'
	post '/segmentation_report' => 'gerals#segmentation_report'
	post '/people_report' => 'gerals#people_report'
	post '/funnel_report' => 'gerals#funnel_report'
	post '/funnel_report_profiles_at_step' => 'gerals#funnel_report_profiles_at_step'

	root 'apps#index'
	resources :apps do
		get :events_form
		get :profiles_form
		get :profiles_report
		get :segmentation_report
		get :people_report
		get :funnel_report
	end
end
